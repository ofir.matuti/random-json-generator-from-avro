package com.ofir.avro.operations;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.ofir.avro.utils.Utils.generateProperties;

public class SearchMessageByKey implements Operation {
    private static final long TIME_TO_SEARCH_IN_MILLIS = 15000;
    private static final long RECORDS_TO_SEARCH_PER_TOPIC = 3000L;
    @Override
    public void execute(String[] args) {
        String topic = args[1],
                bootstrapServer = args[2],
                key = args[3];

        Consumer<String, Object> consumer = new KafkaConsumer<>(generateProperties(bootstrapServer, true));

        List<TopicPartition> partitions = consumer.partitionsFor(topic).stream().map(p -> new TopicPartition(topic, p.partition()))
                .collect(Collectors.toList());

        consumer.assign(partitions);

        consumer.seekToBeginning(Collections.emptySet());
        Map<TopicPartition, Long> startPartitions = partitions.stream().collect(Collectors.toMap(Function.identity(), consumer::position));

        consumer.seekToEnd(Collections.emptySet());
        Map<TopicPartition, Long> endPartitions = partitions.stream().collect(Collectors.toMap(Function.identity(), consumer::position));

        partitions.forEach(p -> consumer.seek(p, calculateOffset(startPartitions,endPartitions,p)));

        System.out.println("searching.. please wait");
        Map<ConsumerRecord<String,Object>,Long> recordsMap = new HashMap<>();

        long finishTime = System.currentTimeMillis() + TIME_TO_SEARCH_IN_MILLIS;
        while(System.currentTimeMillis() < finishTime){
            ConsumerRecords<String, Object> records = consumer.poll(Duration.ofMillis(2500));
            recordsMap.putAll(StreamSupport.stream(records.spliterator(), true).filter(record -> record.key().equals(key))
                    .collect(Collectors.toMap(Function.identity(), ConsumerRecord::timestamp)));
            System.out.print(".");
        }

        takeLastRecordFromMap(recordsMap);
    }

    private void takeLastRecordFromMap(Map<ConsumerRecord<String,Object>,Long> recordsMap){
        if(recordsMap.isEmpty()) {
            System.out.println("\ncould not find message with your key");
            return;
        }
        long maxTimestamp = Collections.max(recordsMap.values());
        System.out.println(beautifyRecord(recordsMap.keySet().stream().filter(k -> recordsMap.get(k)==maxTimestamp).findFirst().get()));
    }
    private long calculateOffset(Map<TopicPartition, Long> startPartitions, Map<TopicPartition, Long> endPartitions, TopicPartition p){
        return Math.max(startPartitions.get(p), endPartitions.get(p) - RECORDS_TO_SEARCH_PER_TOPIC);
    }

    private String beautifyRecord(ConsumerRecord<String,Object> record) {
        return "\npartition: " + record.partition() + "\n" +
                "offset: " + record.offset() + "\n" +
                "key: " + record.key() + "\n" +
                "value: " + record.value().toString();
    }
}


