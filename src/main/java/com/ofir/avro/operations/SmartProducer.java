package com.ofir.avro.operations;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.kafka.clients.producer.*;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.UUID;

import static com.ofir.avro.utils.Utils.*;

public class SmartProducer implements Operation {

    @Override
    public void execute(String[] args) {
        String avscFile = args[1],
                topic = args[2],
                bootstrapServer = args[3],
                jsonFile = args[4],
                keyType = args[5],
                key = args[6];
        Schema avroSchema = SchemaFromAvscFile(avscFile);


        if(keyType.equalsIgnoreCase("string")) {
            KafkaProducer<String, GenericData.Record> kafkaProducer = new KafkaProducer<>(generateProperties(bootstrapServer, true));
            JsonAvroConverter avroConverter = new JsonAvroConverter();
            GenericData.Record genericDataRecord = avroConverter.convertToGenericDataRecord(FileToByteArray(jsonFile), avroSchema);
            ProducerRecord<String, GenericData.Record> record = new ProducerRecord<>(topic, key, genericDataRecord);
            kafkaProducer.send(record, (metadata, ex) -> {
                if (ex == null) {
                    System.out.println("Topic=" + metadata.topic());
                    System.out.println("Partition=" + metadata.partition());
                    System.out.println("Offset=" + metadata.offset());
                    System.out.println("Key=" + key);
                } else {
                    ex.printStackTrace();
                }
            });

            kafkaProducer.flush();
            kafkaProducer.close();
        }
        else if(keyType.equalsIgnoreCase("long")) {
            long keyLong = Long.parseLong(key);
            KafkaProducer<Long, GenericData.Record> kafkaProducer = new KafkaProducer<>(generateProperties(bootstrapServer, false));
            JsonAvroConverter avroConverter = new JsonAvroConverter();
            GenericData.Record genericDataRecord = avroConverter.convertToGenericDataRecord(FileToByteArray(jsonFile), avroSchema);
            ProducerRecord<Long, GenericData.Record> record = new ProducerRecord<>(topic, keyLong, genericDataRecord);
            kafkaProducer.send(record, (metadata, ex) -> {
                if (ex == null) {
                    System.out.println("Topic=" + metadata.topic());
                    System.out.println("Partition=" + metadata.partition());
                    System.out.println("Offset=" + metadata.offset());
                    System.out.println("Key=" + keyLong);
                } else {
                    ex.printStackTrace();
                }
            });

            kafkaProducer.flush();
            kafkaProducer.close();
        }
    }
    private String readFromConsole(BufferedReader reader){
        String keyType = null;
        try {
            keyType = reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return keyType;
    }
}
