package com.ofir.avro.operations;

public interface Operation {
    void execute (String [] args);
}
