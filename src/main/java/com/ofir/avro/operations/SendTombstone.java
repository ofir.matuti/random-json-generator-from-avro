package com.ofir.avro.operations;


import org.apache.avro.generic.GenericData;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.ofir.avro.utils.Utils.generateProperties;

public class SendTombstone implements Operation {
    @Override
    public void execute(String[] args) {
        String topic = args[1];
        String bootstrapServer = args[2];
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Which tombstone would you like to create? [Long/String]");

        String keyType = readFromConsole(reader);
        if(keyType.equalsIgnoreCase("string")){
            System.out.println("enter your String key:");
            String key = readFromConsole(reader);
            KafkaProducer<String, GenericData.Record> kafkaProducer = new KafkaProducer<>(generateProperties(bootstrapServer, true));
            ProducerRecord<String, GenericData.Record> record = new ProducerRecord<>(topic, key,null);
            kafkaProducer.send(record, (metadata, ex) ->  {
                if (ex == null) {
                    beautify(metadata, key);
                } else {
                    ex.printStackTrace();
                }
            });

            kafkaProducer.flush();
            kafkaProducer.close();
        }
        else if(keyType.equalsIgnoreCase("long")){
            System.out.println("enter your Long key:");
            String key = readFromConsole(reader);
            long keyLong = Long.parseLong(key);
            KafkaProducer<Long, GenericData.Record> kafkaProducer = new KafkaProducer<>(generateProperties(bootstrapServer, false));
            ProducerRecord<Long, GenericData.Record> record = new ProducerRecord<>(topic, keyLong,null);
            kafkaProducer.send(record, (metadata, ex) ->  {
                if (ex == null) {
                    beautify(metadata, keyLong + "");
                } else {
                    ex.printStackTrace();
                }
            });

            kafkaProducer.flush();
            kafkaProducer.close();
        }
        else {
            System.out.println("key type is not supported");
        }
    }

    private void beautify(RecordMetadata metadata, String key){
        System.out.println("Topic=" + metadata.topic());
        System.out.println("Partition=" + metadata.partition());
        System.out.println("Offset=" + metadata.offset());
        System.out.println("Key=" + key);
    }

    private String readFromConsole(BufferedReader reader){
        String keyType = null;
        try {
            keyType = reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return keyType;
    }
}
