package com.ofir.avro.operations;

public class OperationFactory {

    public Operation createOperation(String operation){
        Operation e = null;
        if(operation.equalsIgnoreCase("random")){
            e = new GenerateRandomMessage();
        } else if(operation.equalsIgnoreCase("produce")){
            e = new SmartProducer();
        } else if(operation.equalsIgnoreCase("search")){
            e = new SearchMessageByKey();
        } else if(operation.equalsIgnoreCase("tombstone")){
            e = new SendTombstone();
        }
        return e;
    }
}
