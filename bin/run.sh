REGISTRY=http://cd-dv-ny-schemaregistry-general.doubleverify.prod:8081
LOCAL_FOLDER=/tmp/random-json-generator-from-avro

TOPIC=cls-twitter-alpha-classification-response
BOOTSTRAP_SERVER=gd-ms-ue1-kafka-general.doubleverify.prod:9092


SUBJECT=$TOPIC-value


#Generate random
mkdir -p $LOCAL_FOLDER
curl -sSf $REGISTRY/subjects/$SUBJECT/versions/latest | jq -r '.schema' | jq > $LOCAL_FOLDER/file.avsc
java -jar lib/random-json-generator-from-avro.jar random $LOCAL_FOLDER/file.avsc 2>/dev/null > $LOCAL_FOLDER/json

#Produce
java -jar lib/random-json-generator-from-avro.jar 2>/dev/null produce $LOCAL_FOLDER/file.avsc $TOPIC $BOOTSTRAP_SERVER $LOCAL_FOLDER/json long

rm -f $LOCAL_FOLDER/json

#send tombstone
#java -jar lib/random-json-generator-from-avro.jar 2>/dev/null tombstone $TOPIC $BOOTSTRAP_SERVER