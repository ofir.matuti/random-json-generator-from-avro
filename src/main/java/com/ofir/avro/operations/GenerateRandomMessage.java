package com.ofir.avro.operations;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.confluent.avro.random.generator.Generator;
import org.apache.avro.Schema;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;

import static com.ofir.avro.utils.Utils.SchemaFromAvscFile;

public class GenerateRandomMessage implements Operation {
    @Override
    public void execute(String[] args) {
        String avscFile = args[1];
        Schema avroSchema = SchemaFromAvscFile(avscFile);
        Generator generator = new Generator(avroSchema, new Random());
        String generated = null;

        while (generated == null || areTopLevelFieldsNullOrEmpty(generated)) {
            generated = generator.generate().toString();
        }
        System.out.println(generated);
    }

    public static boolean areTopLevelFieldsNullOrEmpty(String jsonString) {
        JSONObject jsonObj = new JSONObject(jsonString);

        for (String key : jsonObj.keySet()) {
            Object value = jsonObj.get(key);
            if (value == null || value.toString().equals("null")) {
                return true;
            }
            if (value instanceof JSONArray) {
                if (((JSONArray) value).isEmpty()) {
                    return true;
                }
            } else if (value instanceof JSONObject) {
                if (((JSONObject) value).isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }
}