if ! command -v mvn &> /dev/null
then
    brew install maven
fi
mvn clean compile assembly:single
sudo cp target/com.ofir.avro-1.0-SNAPSHOT-jar-with-dependencies.jar lib/random-json-generator-from-avro.jar