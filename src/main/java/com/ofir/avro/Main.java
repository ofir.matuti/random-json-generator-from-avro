package com.ofir.avro;

import com.ofir.avro.operations.Operation;
import com.ofir.avro.operations.OperationFactory;
import org.apache.avro.generic.GenericData;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import static com.ofir.avro.utils.Utils.generateProperties;

public class Main {
    public static void main(String [] args) {
        String operation = args[0];
        Operation op = new OperationFactory().createOperation(operation);
        op.execute(args);
    }
}